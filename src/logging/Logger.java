package logging;

public class Logger {
    private static volatile Logger instance = null;
    private Level loggingLevel;

    private Logger() {
        loggingLevel = Level.ALL;
    }

    public static Logger getInstance() {
        if (instance == null) {
            synchronized (Logger.class) {
                if (instance == null) {
                    instance = new Logger();
                }
            }
        }

        return instance;
    }

    public void setLevel(Level loggingLevel) {
        this.loggingLevel = loggingLevel;
    }

    public void log(String message, Level level){
        if(loggingLevel != Level.OFF){
            if (level.intValue() >= loggingLevel.intValue()){
                System.out.printf("%s: %s%n", level, message);
            }
        }
    }
}
