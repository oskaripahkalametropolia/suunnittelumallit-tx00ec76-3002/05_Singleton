package logging;

public enum Level {
    ALL (0),
    DEBUG (1),
    INFO (2),
    WARNING (4),
    ERROR (8),
    OFF (16);

    private final int intValue;

    Level(int i) {
        this.intValue = i;
    }

    int intValue(){
        return intValue;
    }
}
