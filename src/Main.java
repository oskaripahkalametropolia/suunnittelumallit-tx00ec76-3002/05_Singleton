import logging.Level;
import logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        logtest(logger);
        logger.setLevel(Level.DEBUG);
        logtest(logger);
        logger.setLevel(Level.INFO);
        logtest(logger);
        logger.setLevel(Level.WARNING);
        logtest(logger);
        logger.setLevel(Level.ERROR);
        logtest(logger);
        logger.setLevel(Level.OFF);
        logtest(logger);
    }

    private static void logtest(Logger logger){
        logger.log("All test", Level.ALL);
        logger.log("Debug test", Level.DEBUG);
        logger.log("Info test", Level.INFO);
        logger.log("Warning test", Level.WARNING);
        logger.log("Error test", Level.ERROR);
        System.out.println("------------------");
    }
}
